const reducer = (state, action) => {
  switch (action.type) {
    case "ADD_ITEM":
      return {
        cart_count: state.cart_count + 1,
      };

    case "REMOVE_ITEM":
      return {
        cart_count: state.cart_count - 1,
      };

    default:
      return state;
  }
};

export default reducer;
