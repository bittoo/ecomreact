import React from "react";
import Banner from "./Components/Banner/Banner";
import Navigation from "./Components/Navigation/Navigation";
import Courses from "./Containers/Courses/Courses";
import Layout from "./Containers/Layout/Layout";
import { StateProvider } from "./Store";

function App() {
  return (
    <StateProvider>
      <Layout>
        <Navigation />
        <Banner />
        <Courses />
      </Layout>
    </StateProvider>
  );
}

export default App;
