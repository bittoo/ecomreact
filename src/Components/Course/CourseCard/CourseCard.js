import React, { useContext } from "react";
import { Button, Col, Row } from "react-bootstrap";
import Card from "react-bootstrap/Card";
import { store } from "../../../Store";

const CourseCard = (props) => {
  const globalState = useContext(store);
  const { dispatch } = globalState;

  const addToCartHandler = () => {
    dispatch({ type: "ADD_ITEM" });
  };

  return (
    <Col md={4} className="mb-4">
      <Card>
        <Card.Body>
          <Card.Title>{props.title}</Card.Title>
          <Card.Subtitle className="mb-2 text-muted">
            ${props.price}
          </Card.Subtitle>
          <Card.Text>
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </Card.Text>
          <Row>
            <Col md={4}>
              <Card.Link className="align-bottom" href="#">
                Details
              </Card.Link>
            </Col>

            <Col className="text-right">
              <Button variant="primary" onClick={addToCartHandler}>
                Add To Cart
              </Button>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </Col>
  );
};

export default CourseCard;
