import React, { useState, useEffect } from "react";
import Spinner from "react-bootstrap/Spinner";

import Axios from "axios";
import CourseCard from "../../Components/Course/CourseCard/CourseCard";
import { Row } from "react-bootstrap";

const Courses = (props) => {
  const [loading, setLoading] = useState(true);
  const [courses, setCourses] = useState([]);

  useEffect(() => {
    setLoading(true);
    Axios.get("https://reactecom-c009e.firebaseio.com/courses.json").then(
      (response) => {
        const coursesArr = [];
        for (let course_id in response.data) {
          coursesArr.push(response.data[course_id]);
        }

        setCourses(coursesArr);
        setLoading(false);
      }
    );
  }, []);

  let courseList = (
    <Spinner animation="border" role="status" variant="primary">
      <span className="sr-only">Loading...</span>
    </Spinner>
  );
  if (!loading) {
    courseList = courses.map((course) => {
      return (
        <CourseCard key={course.id} title={course.name} price={course.price} />
      );
    });
  }
  console.log("Courses Render");
  return <Row>{courseList}</Row>;
};

export default Courses;
